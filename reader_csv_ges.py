import csv
import folium
import pandas as pd
import json
import pprint
from folium.plugins import MarkerCluster


energie = {'A': 'green',
           'B': 'green',
           'C': 'yellow',
           'D': 'orange',
           'E': 'red',
           'F': 'red',
           'G': 'red',
           'N': 'white'}

grenoble = folium.Map(location=[45.188529, 5.724524], zoom_start=13)
features = {}
get_class = []
get_commune = []
get_ges_commune = {}
get_pourcent_ges_commune = {}
total_ges = 0

with open('49_communes_parse.csv', newline='', encoding="utf-8") as csvfile:
    reader = csv.DictReader(csvfile)
    for row in reader:
        get_class.append(row['classe_estimation_ges'])
        get_commune.append(row['code_insee_commune'])

for commune in get_commune:
    get_ges_commune[commune] = 0
    get_pourcent_ges_commune[commune] = 0

pprint.pprint(get_ges_commune)

clusters = {}

for color_class in pd.unique(get_class):
    features[color_class] = folium.FeatureGroup(name=color_class, show=True)
    clusters[color_class] = MarkerCluster().add_to(features[color_class])

list_display = ['classe_estimation_ges', 'estimation_ges', 'annee_construction', 'surface_habitable', 'numero_dpe']

count = 0
with open('49_communes_parse.csv', newline='', encoding="utf8") as csvfile2:
    reader = csv.DictReader(csvfile2)
    for row in reader:
        count = count + 1
        if row['latitude'] and row['longitude']:
            if float(row['latitude']) > 45 and float(row['latitude']) < 46:
                #count = count + 1
                if row['longitude']:
                    if float(row['longitude']) > 5 and float(row['longitude']) < 6.5:
                        tooltip = "{} {}".format(row['numero_rue'], row['nom_rue'].replace("`", "'"))
                        folium.Marker(
                            [row['latitude'], row['longitude']],
                            popup=["<b>" + elt + "</b>" + ": " + row[elt] + "<br>" for elt in list_display],
                            tooltip=tooltip,
                            icon=folium.Icon(color=energie[row['classe_estimation_ges']])
                        ).add_to(clusters[row['classe_estimation_ges']])
                        print(str(count) + ' rows in map')
                    if row['estimation_ges']:
                        get_ges_commune[row['code_insee_commune']] = get_ges_commune[row['code_insee_commune']] + float(row['estimation_ges'])
                        total_ges = total_ges + float(row['estimation_ges'])


for communes in get_ges_commune:
    get_pourcent_ges_commune[communes] = get_ges_commune[communes] * 100 / total_ges

sub = {}

#for row in clusters:
#    grenoble.add_child(row)
for row in pd.unique(get_class):
#    sub[row] = folium.plugins.FeatureGroupSubGroup(marker_cluster, features[row])
    grenoble.add_child(features[row])
    #features[row].add_to(grenoble)
    #print(row)





geo = json.load(open('perim_com_38.geojson'))


list_gaz = ['Agriculture', 'Autres transports', 'Autres transports international', 'CO2 biomasse hors-total', 'Déchets', 'Energie', 'Industrie hors-énergie', 'Résidentiel', 'Routier', 'Tertiaire']
get_data = {}
get_data2 = {}
count_total_gaz = 0
count_total_autres_transports = 0
count_total_autres_transports_international = 0

with open('IGT - Pouvoir de réchauffement global.csv', newline='') as csvfile3:
    reader = csv.DictReader(csvfile3)

    for row in reader:
        #if row['INSEE commune'].startswith('38'):
        if row['INSEE commune'] in [qu["properties"]["com"] for qu in geo["features"]]:
            #print(row)
            get_data[row['INSEE commune']] = 0
            get_data2[row['INSEE commune']] = {}
            for elt in list_gaz:
                if row[elt]:
                    #print(row[elt])
                    #print(type(row[elt]))
                    a = float(row[elt])
                    get_data[row['INSEE commune']] = get_data[row['INSEE commune']] + a
                    get_data2[row['INSEE commune']][elt] = a
                else:
                    get_data2[row['INSEE commune']][elt] = 0


for row in geo["features"]:
    if row["properties"]["com"] in get_ges_commune.keys():
        #print(row)
        row["properties"]["Total GES"] = get_ges_commune[row["properties"]["com"]]
        row["properties"]["Pourcentage GES"] = get_pourcent_ges_commune[row["properties"]["com"]]
        row["properties"]["Total Gaz"] = get_data[row["properties"]["com"]]


for row in geo["features"]:
    if row["properties"]["com"] in get_data2.keys():
        #print(row)
        #print('OK')
        for key, value in get_data2[row["properties"]["com"]].items():
            row["properties"][key] = value
            row["properties"]['pourcentage_' + key] = value * 100 / get_data[row["properties"]["com"]]


df = pd.DataFrame({
    # "BoroCode" : [qu["properties"]["BoroCode"] - 1 for qu in geo["features"]],
    "Commune": [qu["properties"]["com"] for qu in geo["features"]],
    "GES": [qu["properties"]["Pourcentage GES"] for qu in geo["features"]]
})

for column in map(str,['Commune', 'GES']):
    if column == 'Commune':
        df[column] = df[column].astype(str)
    if column == 'GES':
        df[column] = df[column].astype(float)


choropleth_dpe = folium.Choropleth(
    geo_data=geo,
    name="GES par commune",
    data=df,
    columns=["Commune", "GES"],
    key_on="feature.properties.com",
    fill_color="GnBu",
    nan_fill_color=None,
    fill_opacity=0.7,
    line_opacity=0.2,
    legend_name="Pourcentage GES (%) - Total GES par estimation GES habitation par commune - Total Gaz par secteur par commune avec % du secteur sur le total de la commune",
)

list_test = ['com', 'Total GES', 'Pourcentage GES', 'Total Gaz']
for elt in list_gaz:
    #list_test.append(elt)
    list_test.append('pourcentage_' + elt)

style_function = "font-size: 15px; font-weight: bold"
choropleth_dpe.geojson.add_child(
    folium.features.GeoJsonTooltip(['com'], style=style_function, labels=False))
choropleth_dpe.geojson.add_child(
    folium.features.GeoJsonPopup(fields=list_test)
)

grenoble.add_child(choropleth_dpe)

grenoble.add_child(folium.LayerControl())

export = grenoble.save("carto_ges_38.html")