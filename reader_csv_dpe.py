import csv
import folium
import pandas as pd
import json
import pprint
from folium.plugins import MarkerCluster


energie = {'A': 'green',
           'B': 'green',
           'C': 'yellow',
           'D': 'orange',
           'E': 'red',
           'F': 'red',
           'G': 'red',
           'N': 'white'}

grenoble = folium.Map(location=[45.188529, 5.724524], zoom_start=13)
features = {}
get_class = []
get_commune = []
get_dpe_commune = {}
get_pourcent_dpe_commune = {}
total_dpe = 0

with open('49_communes_parse.csv', newline='', encoding="utf-8") as csvfile:
    reader = csv.DictReader(csvfile)
    for row in reader:
        get_class.append(row['classe_consommation_energie'])
        get_commune.append(row['code_insee_commune'])

for commune in get_commune:
    get_dpe_commune[commune] = 0
    get_pourcent_dpe_commune[commune] = 0

pprint.pprint(get_dpe_commune)

clusters = {}

for color_class in pd.unique(get_class):
    features[color_class] = folium.FeatureGroup(name=color_class, show=True)
    clusters[color_class] = MarkerCluster().add_to(features[color_class])

list_display = ['classe_consommation_energie', 'consommation_energie', 'annee_construction', 'surface_habitable', 'numero_dpe']

count = 0
with open('49_communes_parse.csv', newline='', encoding="utf8") as csvfile2:
    reader = csv.DictReader(csvfile2)
    for row in reader:
        count = count + 1
        if row['latitude'] and row['longitude']:
            if float(row['latitude']) > 45 and float(row['latitude']) < 46:
                #count = count + 1
                if row['longitude']:
                    if float(row['longitude']) > 5 and float(row['longitude']) < 6.5:
                        tooltip = "{} {}".format(row['numero_rue'], row['nom_rue'].replace("`", "'"))
                        folium.Marker(
                            [row['latitude'], row['longitude']],
                            popup=["<b>" + elt + "</b>" + ": " + row[elt] + "<br>" for elt in list_display],
                            tooltip=tooltip,
                            icon=folium.Icon(color=energie[row['classe_consommation_energie']])
                        ).add_to(clusters[row['classe_consommation_energie']])
                        print(str(count) + ' rows in map')
                    if row['consommation_energie']:
                        get_dpe_commune[row['code_insee_commune']] = get_dpe_commune[row['code_insee_commune']] + float(row['consommation_energie'])
                        total_dpe = total_dpe + float(row['consommation_energie'])


for communes in get_dpe_commune:
    get_pourcent_dpe_commune[communes] = get_dpe_commune[communes] * 100 / total_dpe

sub = {}

#for row in clusters:
#    grenoble.add_child(row)
for row in pd.unique(get_class):
#    sub[row] = folium.plugins.FeatureGroupSubGroup(marker_cluster, features[row])
    grenoble.add_child(features[row])
    #features[row].add_to(grenoble)
    #print(row)

geo = json.load(open('perim_com_38.geojson'))

for row in geo["features"]:
    if row["properties"]["com"] in get_dpe_commune.keys():
        #print(row)
        row["properties"]["Total DPE"] = get_dpe_commune[row["properties"]["com"]]
        row["properties"]["Pourcentage DPE"] = get_pourcent_dpe_commune[row["properties"]["com"]]

df = pd.DataFrame({
    # "BoroCode" : [qu["properties"]["BoroCode"] - 1 for qu in geo["features"]],
    "Commune": [qu["properties"]["com"] for qu in geo["features"]],
    "DPE": [qu["properties"]["Pourcentage DPE"] for qu in geo["features"]]
})

for column in map(str,['Commune', 'DPE']):
    if column == 'Commune':
        df[column] = df[column].astype(str)
    if column == 'DPE':
        df[column] = df[column].astype(float)


choropleth_dpe = folium.Choropleth(
    geo_data=geo,
    name="DPE par commune",
    data=df,
    columns=["Commune", "DPE"],
    key_on="feature.properties.com",
    fill_color="YlOrRd",
    nan_fill_color=None,
    fill_opacity=0.7,
    line_opacity=0.2,
    legend_name="Pourcentage DPE (%) - Total DPE (kWhEP/m².an)",
)

list_test = ['com', 'Total DPE', 'Pourcentage DPE']

style_function = "font-size: 15px; font-weight: bold"
choropleth_dpe.geojson.add_child(
    folium.features.GeoJsonTooltip(['com'], style=style_function, labels=False))
choropleth_dpe.geojson.add_child(
    folium.features.GeoJsonPopup(fields=list_test)
)

grenoble.add_child(choropleth_dpe)

grenoble.add_child(folium.LayerControl())

export = grenoble.save("carto_dpe_38.html")